import configparser


class LoadConfiguration(configparser.RawConfigParser):
    baseline = None
    experiments = None

    def __init__(self, baseFileName='baseline.ini',
                 experimentsFileName='experiments.ini'):
        configparser.RawConfigParser.__init__(self)

        self.optionxform = str  # Do not lowercase key elements!

        self.baseFileName = baseFileName
        self.experimentsFileName = experimentsFileName

        self.getBaseLine()

        self.getExperiments()

    def getBaseLine(self):
        # read the baseline file
        self.read(self.baseFileName)
        print(self.sections())
        # baseline file has single section
        sec = self.sections()[0]

        self.baseline = {}
        # Iterate over items and calculate their "value"
        for item in self.items(sec):

            if item[0] == 'transmissionRatesHIV':
                # beta_F2M, beta_M2F
                self.baseline['beta_M2F'] = self.readVal(item[1])
                self.baseline['beta_F2M'] = self.baseline['beta_M2F']
            self.baseline[item[0]] = self.readVal(item[1])

        self.remove_section(sec)

        # print self.baseline

    def getExperiments(self):

        self.read(self.experimentsFileName)

        self.experiments = {}
        experimentList = self.sections()
        # print self.baseline

        for i in range(len(experimentList)):
            # print experimentList[i]
            # if experimentList[i] == 'default':
            #    continue

            self.experiments[experimentList[i]] = self.baseline.copy()

            # set default values from section [default] in experiments.ini
            for item in self.items('default'):
                # print item
                self.experiments[experimentList[i]
                                 ][item[0]] = self.readVal(item[1])

            for item in self.items(experimentList[i]):
                # print item
                self.experiments[experimentList[i]
                                 ][item[0]] = self.readVal(item[1])

            # set name of the scenario:
            # sscenarioname
            self.experiments[experimentList[i]
                             ]['sScenarioName'] = experimentList[i]

    def readVal(self, cval):
        """
        Use exec() to get real value of the input: int, float, list, string...
        """
        try:
            ps = "cvalue = " + cval
            # print ps
            exec(ps)
            return cvalue
        except BaseException:
            return cval
