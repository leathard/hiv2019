"""
Extend class isaac.concurrency.Infection

Infection was originally written by Alan G. Isaac. It began as a replication of Eaton,
Hallet, and Garnett (2010) and is very heavily influenced by
their online C++ code (person.cpp and partnership.cpp).
http://www.springerlink.com/content/22t477k235477463/10461_2010_Article_9787_ESM.html

New class is compatible with new Person class.
New class accounts for different transmission rates for agents on treatment
(ART/PrEP).
Overrides expose() method.

"""

import logging
# clean import of ./isaac/concurrency.py/Infection class only
from .isaac.concurrency import Infection as aiInfection


# stagedHIVfactory had to be copied from isaac.
def stagedHIVfactory(durations, transM2F, transF2M):
    """Return class for staged HIV infection.
    (We adopt this strategy to allow multiprocessing,
    since class variables must be assigned at runtime.)
    """
    class HIV(Infection):
        _durations = tuple(durations)
        _duration = sum(_durations)
        _beta_M2F = tuple(transM2F)
        _beta_F2M = tuple(transF2M)
        is_fatal = True
        assert len(_durations) == len(_beta_F2M) == len(_beta_M2F)
    return HIV


class Infection(aiInfection):
    """init class"""

    def __init__(self, host, day, registry):
        aiInfection.__init__(self, host, day, registry)
        # print self

    def __str__(self):
        """print object properties"""
        return """
        I am a {cls} class.
        """.format(**dict(cls=self.__class__))

    # called by Partnership::expose_transmission
    def expose(self, partnership, day):
        """Return None; expose a *partnership*;
        and possibly schedule a transmission event.

        We expose the partnership to infection
        by sampling a candidate transmission date during
        each stage based on the transmission rate during that stage,
        checking to see if the candidate date is before the end of that stage
        and before the end of the partnership.
        If so, transmission is scheduled for that date.
        If not, proceed to the next stage.
        If transmission is not scheduled during any stage,
        no transmission occurs during the partnership.

        Called by `Partnership.expose_transmission`,
        which is called upon partnership formation or upon infection.

        :comment:
            We schedule a transmission only if serodiscordant
            (exactly one of the two is infected),
            but with concurrency another partner might still infect
            the uninfected partner before this scheduled transmission happens.
        :note:
            override to include ART and PrEP
        """
        male, female = partnership.partners
        assert male.is_infected != female.is_infected
        sexfreq = partnership.sexfreq

        # print "Registry: \n", self.registry.params
        beta_ART = self.registry.params['transmissionRatesHIV_ART']
        beta_PrEP = self.registry.params['transmissionRatesHIV_PREP']

        if male.is_infected:
            # male might use ART and female PrEP
            if male.ART and female.PrEP:
                beta = ((i[0] * i[1]) for i in zip(beta_ART, beta_PrEP))
            elif male.ART:
                beta = beta_ART
            else:
                beta = self._beta_M2F

        if female.is_infected:
            # female might use ART and male PrEP
            if female.ART and male.PrEP:
                beta = (i[0] * i[1] for i in zip(beta_ART, beta_PrEP))
            elif female.ART:
                beta = beta_ART
            else:
                beta = self._beta_F2M
        beta_p, beta_a, beta_s, beta_0 = (sexfreq * beta_i for beta_i in beta)
        dur_p, dur_a, dur_s, dur_0 = self._durations
        candidate = eligDate = day  # candidate transmission day
        if(eligDate >= partnership.end_date):
            assert eligDate == partnership.end_date
            return

        stageEndDate = self.start_date  # initialize to date of infection
        schedule = self.registry
        for (dur_i, beta_i) in [(dur_p, beta_p),
                                (dur_a, beta_a), (dur_s, beta_s), ]:
            stageEndDate += dur_i  # infection period (increment then compare)
            print((
                "eligDate: {}, stageEndDate: {}".format(
                    eligDate, stageEndDate)))
            print(("beta_i: {}".format(beta_i)))
            if(eligDate < stageEndDate):
                # daily transmission probability during this stage
                if(beta_i > 0):
                    candidate = partnership.candidate(eligDate, beta_i)
                    print(("candidate is: {}".format(candidate)))
                    if(candidate <= stageEndDate and candidate <= partnership.end_date):
                        # schedule a transmission
                        partnership.transmission_scheduled = True
                        partnership.transmission_date = candidate
                        schedule.register_transmission(partnership)
                        logging.info(
                            'Infection.expose: Transmission scheduled')
                        return
                eligDate = stageEndDate  # if beta_i=0 or candidate too far out
            if(eligDate >= partnership.end_date):
                return

        stageEndDate += dur_0
        if(eligDate < stageEndDate and beta_0 > 0):
            candidate = partnership.candidate(eligDate, beta_0)
            if(candidate <= stageEndDate and candidate <= partnership.end_date):
                partnership.transmission_scheduled = True
                partnership.transmission_date = candidate
                schedule.register_transmission(partnership)
                return
