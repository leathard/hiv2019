"""
Extend class isaac.concurrency.Person.
Introcude treatment and type attributes.
Treatment = 0 (None), 1 (ART), 2 (PrEP).
Type = 'common' person vs core group person.
Core groups: commercial sex workers (CSW) and miners.
"""
# clean import of the ./isaac/concurrency.py/Person class ONLY
from .isaac.concurrency import Person as aiPerson  # change name to avoid problems


class Person(aiPerson):
    # init the class
    def __init__(self,
                 sex,
                 registry,
                 params,
                 person_type=0,
                 # 0=> common person, 1 => miner (only males), 2 => CSV (only
                 # females)
                 treatment=0,  # treatment => 0 (None), 1 (ART), 2 (PrEP)
                 CSWclient=False  # CSW client? Only males.
                 ):

        # Init parent class
        aiPerson.__init__(self, sex, registry, params)
        self.person_type = person_type
        self.treatment = treatment
        self.CSWclient = CSWclient

    # print object properties
    def __str__(self):
        return """
        I am a {sex} {cls} who is{CSW} engaged in sex work.
        I receive {treatment} treatment.
        """.format(**dict(sex=self.sex,
                          cls=self.__class__,
                          CSW="" if self.CSW else " not",
                          treatment="no" if self.treatment == 0 else("ART" if self.ART else "PrEP")))

    @property
    def miner(self):  # HIV drug
        return 1 == self.person_type

    @miner.setter
    def miner(self, value):
        assert isinstance(
            value, bool) and self.sex == 'M', "Wrong input type for this person"
        self.person_type = 1 if value else 0

    # ai: having this setter suggests you will not be assigning the miner type at creation;
    #    is there are reason for making this mutable? (Same comments on other types.)
    # jk: My understanding it that changing mutable objects is cheap, avoids creating a copy.
    #    Immutable objects are fundamentally expensive to change. With
    #    uncertainty of what, if any, future work will need to be done I went
    #    with mutable. This seems more stepwise to me.
    # ai20180606: Let me put this a different way: do you use this setter?  If not, you should comment
    #    it out until you need it. (And it is usually (but not always) best if you will never need it.)
    #    If you do use it, please explain where and why.
    # jk: I see. You are correct, I do not ever use the setter as
    #   <self.person_type> and it does not have to be in the <def__init__()>
    #   function. It was a personal preference on readability and ensuring that
    #   one attribute compactly keeps attributes of Person, i.e. ensuring Person
    #   can't be miner and CSW at the same time. Might be pedestrian and
    #   unecessary, but I can check that female.CSW returns True if
    #   person_teyp==2 or female.CSW=True is equivalent with
    #   female.person_type=2. Happy to comment out.
    # ai20181231: 1. that check seems to require only the getter, not the
    # setter.

    @property
    def CSW(self):
        return 2 == self.person_type

    @CSW.setter
    def CSW(self, value):
        assert isinstance(
            value, bool) and self.sex == 'F', "Wrong input type for this person"
        if value:
            self.person_type = 2
        else:
            self.person_type = 0

    @property
    def ART(self):  # HIV drug
        return 1 == self.treatment

    @ART.setter
    def ART(self, value):
        assert isinstance(value, bool), "Wrong input type for ART"
        if value:
            self.treatment = 1
        else:
            """
            :TODO:
            Set to treatment 0? Check if something else was set?
            Report error?
            """
            self.treatment = 0

    @property
    def PrEP(self):  # HIV prevention
        return 2 == self.treatment

    @PrEP.setter
    def PrEP(self, value):
        assert isinstance(value, bool), "Wrong input type for PrEP"
        if value:
            self.treatment = 2
        else:
            """
            :todo:
            Set to treatment 0? Check if something else was set?
            Report error?
            """
            self.treatment = 0

    def is_available(self, yearDay=None):
        # Probably better to just pass the day and compute the yearDay here
        return yearDay >= self.activeRange[0] and yearDay < self.activeRange[-1]
