"""
This file is used to run multiple simulations.
How to run:

python simRun.py <experimentName> mp

<experimentName> is section name from file experiments.ini
add mp as third argument to use multiprocessing

"""
import sys
# from o_sim import run_simulations, run_simulations_mp, onesim
# changed model_phi to sPhi
# from isaac.params import phi_ehg
# import logging

# IMPORTANT: GET PARAMS FOR THIS **MODEL**:  SHARED BY SIMULATIONS!
from . import loadConfiguration


if __name__ == '__main__':
    # print(sys.argv)
    if len(sys.argv) == 1:
        sys.argv.append('Full')
        sys.argv.append('mp')
    # print(sys.argv)
    experimentName = sys.argv[1]

    # Load all configurations
    # baseFileName='baseline.ini', experimentsFileName = 'experiments.ini'
    par = loadConfiguration.LoadConfiguration()
    # select experiment
    try:
        model_params = par.experiments[experimentName]
        # print model_params
    except Exception as e:
        print(("Attempt to get parameters of the experiment '%s' failed due exception:\n%s" % (experimentName, e, )))
        exit()

    model_params['outfolder'] = 'temp'
    model_params['model_name'] = 'test'
    model_params['model_phi'] = locals()[
        model_params['sPhi']]  # select function phi_ehg
    # print model_params
    if len(sys.argv) == 3:
        run_simulations_mp(model_params)
    else:
        run_simulations(model_params)
