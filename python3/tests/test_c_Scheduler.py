"""
Unit test for c_Person. Establish the getter for the boolean behaves correctly
over the input ranges.

"""

import pytest
import loadConfiguration
import configparser
from c_Scheduler import Scheduler
from c_Person import Person
from c_Partnership import Partnership
from c_Infection import Infection, stagedHIVfactory
from isaac.parameters import Scenarios


def make_disease_cls(registry):
    dur_p = 5
    dur_a = 0
    dur_s = 0
    dur_0 = 0
    durations = [dur_p, dur_a, dur_s, dur_0]

    transM2F = [0.00058520544, 2.323288e-05, 0.00016657536000000002, 0]
    transF2M = [0.00058520544, 2.323288e-05, 0.00016657536000000002, 0]
    infect_cls = stagedHIVfactory(durations, transM2F, transF2M)
    return infect_cls


def make_disease(host, day, registry, is_fatal):
    infect_cls = make_disease_cls(registry)

    disease = infect_cls(host, day, registry)
    disease.end_date = day
    print(disease)
    # disease.host = host
    disease.is_fatal = is_fatal
    return disease


def make_params():
    from isaac.params import phi_ehg
    args = []
    args.append('Full')
    args.append('mp')
    # print(sys.argv)
    experimentName = args[0]

    # Load all configurations
    # baseFileName='baseline.ini', experimentsFileName = 'experiments.ini'
    par = loadConfiguration.LoadConfiguration()
    # select experiment
    try:
        model_params = par.experiments[experimentName]
        # print model_params
    except Exception as e:
        print(("Attempt to get parameters of the experiment '%s' failed due exception:\n%s" % (experimentName, e)))
        exit()

    model_params['outfolder'] = 'temp'
    model_params['model_name'] = 'test'
    model_params['model_phi'] = locals()[
        model_params['sPhi']]  # select function phi_ehg
    return model_params


def make_scenarios():
    fscenarios = "scenarios.ini"
    scenarios = Scenarios("baseline.ini", fscenarios)
    return scenarios


def make_prng_params():
    from numpy.random import RandomState
    seed = 51
    pRNG = RandomState(seed=seed)
    return dict(prng=pRNG)


def make_person(sex, registry, is_infected=True, treatment=0):
    from numpy.random import RandomState
    seed = 51
    pRNG = RandomState(seed=seed)
    person = Person(
        sex=sex,
        registry=None,
        params=make_prng_params(),
        treatment=treatment)
    person.is_infected = is_infected
    return person


def make_sim_params():
    from isaac.params import shared_params
    return shared_params


def make_partnership(male, female, registry, params, day=0, tag='comsex'):
    prng_params = make_prng_params()
    newpartnership = Partnership(
        male,
        female,
        day,
        registry=registry,
        params=prng_params,
        tag=tag)
    return newpartnership


def test_scheduler_form_partnerships():
    params = make_params()
    sim_params = make_sim_params()
    scheduler = Scheduler(sim_params)
    males = [
        make_person('M', scheduler),
        make_person('M', scheduler),
        make_person('M', scheduler)
    ]
    females = [
        make_person('F', scheduler),
        make_person('F', scheduler),
        make_person('F', scheduler)
    ]
    days = None
    # cannot test because this uses a random generator
    # scheduler.form_partnerships(males,females,day)


def test_scheduler_transmissions():
    params = make_params()
    sim_params = make_sim_params()
    scheduler = Scheduler(sim_params)
    male = make_person('M', scheduler)
    female = make_person('F', scheduler)

    partnership = make_partnership(male, female, scheduler, params)
    partnership.transmission_date = 0
    scheduler.register_transmission(partnership)


def test_scheduler_partnership_remove():
    params = make_params()
    sim_params = make_sim_params()
    scheduler = Scheduler(sim_params)
    params['Disease'] = make_disease_cls(scheduler)
    # try to remove scheduled and not scheduled
    scheduler = Scheduler(params)
    male = make_person('M', scheduler)
    female = make_person('F', scheduler)

    partnership = make_partnership(male, female, scheduler, params)
    partnership.transmission_date = 0
    partnership.transmission_scheduled = True
    scheduler.register_transmission(partnership)

    print((scheduler.transmissions[partnership.transmission_date]))
    print((scheduler.partnerships))
    scheduler.hiv_transmissions(partnership.transmission_date)
    print((scheduler.transmissions[partnership.transmission_date]))
    print((scheduler.partnerships))
    assert partnership not in scheduler.transmissions[partnership.transmission_date]
    assert partnership in scheduler.partnerships

    scheduler = Scheduler(sim_params)
    male = make_person('M', scheduler)
    female = make_person('F', scheduler)
    partnership = make_partnership(male, female, scheduler, params)
    partnership.transmission_date = 0
    partnership.transmission_scheduled = False
    scheduler.register_transmission(partnership)
    assert partnership in scheduler.transmissions[partnership.transmission_date]
    assert partnership in scheduler.partnerships


def test_scheduler_dissolutions():
    # should remove partnership and also the dissolution for the set day
    params = make_params()
    sim_params = make_sim_params()
    # try to remove scheduled and not scheduled
    scheduler = Scheduler(sim_params)
    male1 = make_person('M', scheduler)
    female1 = make_person('F', scheduler)
    partnership1 = make_partnership(male1, female1, scheduler, params)
    partnership1.transmission_date = 0
    partnership1.transmission_scheduled = False

    male2 = make_person('M', scheduler)
    female2 = make_person('F', scheduler)
    # no registry so we can register partership
    partnership2 = make_partnership(male2, female2, None, params)
    partnership2.transmission_date = 1  # test the date of transmission
    partnership2.transmission_scheduled = False
    # use a fake end_date so this does not get removed by dissolve_partnerships
    partnership2.end_date = 2
    scheduler.register_partnership(partnership2)

    day_to_test = 1
    scheduler.dissolve_partnerships(day_to_test)
    print(("end date is ", partnership1.end_date))
    assert partnership1 not in scheduler.dissolutions[day_to_test]
    assert partnership1 not in scheduler.partnerships
    assert partnership2 in scheduler.partnerships


def test_scheduler_deaths():
    # register an infection on a set date then test the hiv_deaths call
    params = make_params()
    sim_params = make_sim_params()
    # try to remove scheduled and not scheduled
    scheduler = Scheduler(sim_params)

    fatal_day_to_end = 1
    non_fatal_day_to_end = 2

    male1 = make_person('M', scheduler)
    host = male1
    # dont set registry to avoid test choking. call register_infection after
    # disease is created
    fatal_disease = make_disease(male1, fatal_day_to_end, None, True)
    print(("fatal_disease day to end is: " + str(fatal_disease.end_date)))
    scheduler.register_infection(fatal_disease)

    male2 = make_person('M', scheduler)
    non_fatal_disease = make_disease(male2, non_fatal_day_to_end, None, False)
    # scheduler.register_infection(non_fatal_disease)

    print((scheduler.deaths))
    day_to_test = fatal_day_to_end
    assert male1 in scheduler.deaths[day_to_test]
    scheduler.hiv_deaths(fatal_day_to_end)
    assert male1 not in scheduler.deaths[day_to_test]
    assert male2 not in scheduler.deaths[day_to_test]


def test_scheduler_start_treatment():
    params = make_params()
    sim_params = make_sim_params()
    scheduler = Scheduler(sim_params)
    males = [
        make_person('M', scheduler)
    ]
    females = [
        make_person('F', scheduler, treatment=1)
    ]
    day = 1
    scheduler.start_treatment(males, females, day, params)
    assert males[0].ART is False
    assert females[0].ART
    assert males[0].PrEP is False
    assert females[0].PrEP is False

    scheduler = Scheduler(sim_params)
    males = [
        make_person('M', scheduler, treatment=1)
    ]
    females = [
        make_person('F', scheduler)
    ]
    day = 1
    scheduler.start_treatment(males, females, day, params)
    assert males[0].ART
    assert females[0].ART is False
    assert males[0].PrEP is False
    assert females[0].PrEP is False
