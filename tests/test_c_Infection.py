"""
Unit test for c_Person. Establish the getter for the boolean behaves correctly
over the input ranges.

"""

import pytest
import sys
import loadConfiguration
from c_Infection import Infection, stagedHIVfactory
from c_Person import Person
from c_Partnership import Partnership
from c_Scheduler import Scheduler


def make_sim_params():
    from isaac.params import shared_params
    return shared_params


def make_prng_params():
    from numpy.random import RandomState
    seed = 51
    pRNG = RandomState(seed=seed)
    return dict(prng=pRNG)


def make_person(sex, registry, treatment=1):
    from numpy.random import RandomState
    seed = 51
    pRNG = RandomState(seed=seed)
    person = Person(
        sex=sex,
        registry=None,
        params=make_prng_params(),
        treatment=treatment)
    # person.is_infected = True
    return person


def make_person_2():  # for the infect test
    from numpy.random import RandomState
    seed = 51
    pRNG = RandomState(seed=seed)
    person = Person(sex='M', registry=None, params=dict(prng=pRNG))
    person.is_infected = True
    return person


def make_mock_infection(durations, host, day, registry):
    transM2F = [0.00058520544, 2.323288e-05, 0.00016657536000000002, 0]
    transF2M = [0.00058520544, 2.323288e-05, 0.00016657536000000002, 0]
    infect_cls = stagedHIVfactory(durations, transM2F, transF2M)
    return infect_cls(host, day, registry)


def make_params():
    from isaac.params import phi_ehg
    args = []
    args.append('Full')
    args.append('mp')
    # print(sys.argv)
    experimentName = args[0]

    # Load all configurations
    # baseFileName='baseline.ini', experimentsFileName = 'experiments.ini'
    par = loadConfiguration.LoadConfiguration()
    # select experiment
    try:
        model_params = par.experiments[experimentName]
        # print model_params
    except Exception as e:
        print("Attempt to get parameters of the experiment '%s' failed due exception:\n%s" % (experimentName, e))
        exit()

    model_params['outfolder'] = 'temp'
    model_params['model_name'] = 'test'
    model_params['model_phi'] = locals()[
        model_params['sPhi']]  # select function phi_ehg
    return model_params


def make_disease_cls(registry):
    dur_p = 5
    dur_a = 0
    dur_s = 0
    dur_0 = 0
    durations = [dur_p, dur_a, dur_s, dur_0]

    transM2F = [0.00058520544, 2.323288e-05, 0.00016657536000000002, 0]
    transF2M = [0.00058520544, 2.323288e-05, 0.00016657536000000002, 0]
    infect_cls = stagedHIVfactory(durations, transM2F, transF2M)
    return infect_cls


def make_disease(host, day, registry):
    day = 5
    infect_cls = make_disease_cls()
    return infect_cls(host, day, registry)


def test_infection_stage():
    # test for all stages of infection
    # days infected should always be days - infection start date
    day_to_stage = 10
    host = make_person_2()
    registry = None

    dur_p = 5
    dur_a = 0
    dur_s = 0
    dur_0 = 0
    durations = [dur_p, dur_a, dur_s, dur_0]
    infection_day = 5
    host = make_person_2()
    registry = None
    infection = make_mock_infection(durations, host, infection_day, registry)
    # test for 'primary'
    # on dur_p
    assert infection.stage(day_to_stage) == 'primary'

    dur_p = -1
    dur_a = 1
    dur_s = -3
    dur_0 = 0
    durations = [dur_p, dur_a, dur_s, dur_0]
    infection_day = 1
    host = make_person_2()
    registry = None
    infection = make_mock_infection(durations, host, infection_day, registry)
    day_to_stage = 1
    # test for 'asymptomatic'
    # check for days_infected <= dur_p+dur_a
    assert infection.stage(day_to_stage) == 'asymptomatic'

    dur_p = 1
    dur_a = 1
    dur_s = 1
    dur_0 = 0
    durations = [dur_p, dur_a, dur_s, dur_0]
    infection_day = 1
    host = make_person_2()
    registry = None
    infection = make_mock_infection(durations, host, infection_day, registry)
    day_to_stage = 1
    day_to_stage = 4
    # test for 'sympotmatic'
    # check for days_infected <= dur_p+dur_a
    assert infection.stage(day_to_stage) == 'sympotmatic'

    dur_p = 1
    dur_a = 1
    dur_s = 1
    dur_0 = 1
    durations = [dur_p, dur_a, dur_s, dur_0]
    infection_day = 1
    host = make_person_2()
    registry = None
    infection = make_mock_infection(durations, host, infection_day, registry)
    day_to_stage = 6
    # test for 'dying'
    assert infection.stage(day_to_stage) == 'dying'


def test_infection_expose():
    # uses a random number generator
    # todo please add sample test cases
    params = make_params()
    sim_params = make_sim_params()
    registry = Scheduler(params)

    male = make_person('M', registry)
    female = make_person('F', registry)
    days = 365  # same as nBurnDays
    disease = make_disease_cls(registry)

    infect_day = 6001
    male.infect(disease, infect_day, registry)   # infect the male
    tag = 'comsex'
    day = 6000
    newpartnership = Partnership(
        male,
        female,
        day,
        registry=registry,
        params=make_prng_params(),
        tag=tag)

    dur_p = 10
    dur_a = 10
    dur_s = 10
    dur_0 = 10

    durations = [dur_p, dur_a, dur_s, dur_0]
    infection_day = 1930
    host = male
    infection = make_mock_infection(durations, host, infection_day, registry)
    expose_day = 6000
    infection.expose(newpartnership, expose_day)
    assert newpartnership.transmission_scheduled is False

    params = make_params()
    sim_params = make_sim_params()
    registry = Scheduler(params)

    male = make_person('M', registry, treatment=2)
    male.miner = True
    female = make_person('F', registry, treatment=2)
    days = 365  # same as nBurnDays
    disease = make_disease_cls(registry)
    female.infect(disease, 1, registry)   # infect the male

    # use the primary tag this time and set some defaults based on
    # the sample ini
    tag = 'primary'
    day = 2000
    params = make_prng_params()
    params["sigma01"] = 0.005
    params["days_mining"] = 165
    newpartnership = Partnership(
        male,
        female,
        day,
        registry=registry,
        params=params,
        tag=tag)

    dur_p = 9000000
    dur_a = 9000000
    dur_s = 9000000

    dur_0 = 9000000

    durations = [dur_p, dur_a, dur_s, dur_0]
    infection_day = 1
    host = male
    infection = make_mock_infection(durations, host, infection_day, registry)
    expose_day = 2
    infection.expose(newpartnership, expose_day)
    assert newpartnership.transmission_scheduled
