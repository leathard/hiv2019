"""
Unit test for c_Person. Establish the getter for the boolean behaves correctly
over the input ranges.

"""

import pytest
import loadConfiguration
from c_Partnership import Partnership
from c_Person import Person
from c_Infection import stagedHIVfactory
from c_Scheduler import Scheduler
from o_sim import get_param_set


def make_params():
    from isaac.params import phi_ehg
    args = []
    args.append('Full')
    args.append('mp')
    # print(sys.argv)
    experimentName = args[0]

    # Load all configurations
    # baseFileName='baseline.ini', experimentsFileName = 'experiments.ini'
    par = loadConfiguration.LoadConfiguration()
    # select experiment
    try:
        model_params = par.experiments[experimentName]
        # print model_params
    except Exception as e:
        print("Attempt to get parameters of the experiment '%s' failed due exception:\n%s" % (experimentName, e))
        exit()

    model_params['outfolder'] = 'temp'
    model_params['model_name'] = 'test'
    print("params are ", model_params)
    model_params['model_phi'] = locals()[
        model_params['sPhi']]  # select function phi_ehg
    return model_params


def make_scheduler():
    params = make_params()
    return Scheduler(params)


def make_disease_cls(registry):
    dur_p = 5
    dur_a = 0
    dur_s = 0
    dur_0 = 0
    durations = [dur_p, dur_a, dur_s, dur_0]

    transM2F = [0.00058520544, 2.323288e-05, 0.00016657536000000002, 0]
    transF2M = [0.00058520544, 2.323288e-05, 0.00016657536000000002, 0]
    infect_cls = stagedHIVfactory(durations, transM2F, transF2M)
    return infect_cls


def make_disease(host, day, registry):
    day = 5
    infect_cls = make_disease_cls()
    return infect_cls(host, day, registry)


def make_prng_params():
    from numpy.random import RandomState
    seed = 51
    pRNG = RandomState(seed=seed)
    return dict(prng=pRNG)


def make_person(sex, registry):
    from numpy.random import RandomState
    seed = 51
    pRNG = RandomState(seed=seed)
    person = Person(sex=sex, registry=None, params=make_prng_params())
    # person.is_infected = True
    return person


def test_partnership_nburndays():
    params = make_params()
    registry = None
    male = make_person('M', registry)
    female = make_person('F', registry)
    tag = 'comsex'
    day = 365  # same as nBurnDays
    newpartnership = Partnership(
        male,
        female,
        day,
        registry=registry,
        params=make_prng_params(),
        tag=tag)
    assert newpartnership in male.partnerships
    assert newpartnership in female.partnerships


def test_partnership_infection():
    params = make_params()
    registry = make_scheduler()
    male = make_person('M', registry)
    female = make_person('F', registry)

    day = 365  # same as nBurnDays
    disease = male.infect(
        make_disease_cls(registry),
        day,
        registry)   # infect the male
    tag = 'comsex'
    day = 365  # same as nBurnDays
    newpartnership = Partnership(
        male,
        female,
        day,
        registry=registry,
        params=make_prng_params(),
        tag=tag)
    # todo need to check expose_transmission


def test_partnership_transmit():
    params = make_params()

    registry = make_scheduler()
    male = make_person('M', registry)
    female = make_person('F', registry)

    day = 365  # same as nBurnDays
    disease_cls = make_disease_cls(registry)
    disease = male.infect(disease_cls, day, registry)   # infect the male
    tag = 'comsex'
    newpartnership = Partnership(
        male,
        female,
        day,
        registry=registry,
        params=make_prng_params(),
        tag=tag)
    transmit_day = 10
    newpartnership.transmission_scheduled = True
    newpartnership.transmission_date = transmit_day
    transmitter, disease = newpartnership.transmit(disease_cls, transmit_day)
    assert transmitter == male


def test_partnership_minerProlongation():
    params = make_params()

    registry = make_scheduler()
    male = make_person('M', registry)
    female = make_person('F', registry)

    day = 365  # same as nBurnDays
    disease_cls = make_disease_cls(registry)
    disease = male.infect(disease_cls, day, registry)   # infect the male
    tag = 'comsex'
    params = make_prng_params()
    params["sigma01"] = 0.005
    params["days_mining"] = 165

    newpartnership = Partnership(
        male,
        female,
        day,
        registry=registry,
        params=params,
        tag=tag)
    day = 1
    days_to = 10
    day_to_test = 10
    assert day_to_test == newpartnership.minerProlongation(day, days_to)
    day = 5
    days_to = 20
    day_to_test = 20
    assert day_to_test == newpartnership.minerProlongation(day, days_to)
