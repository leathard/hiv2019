"""
Unit test for c_Person. Establish the getter for the boolean behaves correctly
over the input ranges.

"""

import pytest
from c_Person import Person


def test_miner():
    """
    test miner class
    """
    p00 = Person('M', None, dict(prng=None), person_type=0)
    assert p00.miner is False
    p01 = Person('M', None, dict(prng=None), person_type=1)
    assert p01.miner is True
    p02 = Person('M', None, dict(prng=None), person_type=2)
    assert p02.miner is False


def test_miner_setter_exception():
    p = Person('F', None, dict(prng=None), treatment=1)
    with pytest.raises(AssertionError):
        p.miner = True


def test_CSW():
    """
    test CSW class
    """
    p00 = Person('F', None, dict(prng=None), person_type=0)
    assert p00.CSW is False
    p01 = Person('F', None, dict(prng=None), person_type=1)
    assert p01.CSW is False
    p02 = Person('F', None, dict(prng=None), person_type=2)
    assert p02.CSW


def test_CSW_setter_exception():
    p = Person('M', None, dict(prng=None), treatment=1)
    with pytest.raises(AssertionError):
        p.CSW = True


def test_ART():
    """
    test ART/treatment class for males
    """
    p00 = Person('M', None, dict(prng=None), treatment=1)
    assert p00.ART
    p01 = Person('M', None, dict(prng=None), treatment=2)
    assert p01.ART is False
    p02 = Person('M', None, dict(prng=None), treatment=0)
    assert p02.ART is False


def test_ART():
    """
    test ART/treatment class for females
    """
    p00 = Person('F', None, dict(prng=None), treatment=1)
    assert p00.ART
    p01 = Person('F', None, dict(prng=None), treatment=2)
    assert p01.ART is False
    p02 = Person('F', None, dict(prng=None), treatment=0)
    assert p02.ART is False


def test_PrEP():
    """
    test ART/treatment class for males
    """
    p00 = Person('M', None, dict(prng=None), treatment=1)
    assert p00.PrEP is False
    p01 = Person('M', None, dict(prng=None), treatment=2)
    assert p01.PrEP is True
    p02 = Person('M', None, dict(prng=None), treatment=0)
    assert p02.PrEP is False


def test_ART():
    """
    test ART/treatment class for females
    """
    p00 = Person('F', None, dict(prng=None), treatment=1)
    assert p00.PrEP is False
    p01 = Person('F', None, dict(prng=None), treatment=2)
    assert p01.PrEP is True
    p02 = Person('F', None, dict(prng=None), treatment=0)
    assert p02.PrEP is False
